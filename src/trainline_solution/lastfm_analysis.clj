(ns trainline-solution.lastfm-analysis
  (:require [clojure.test :refer :all]
      [sparkling.conf :as conf]
      [sparkling.core :as spark]
      [sparkling.destructuring :as s-de]
      [clj-time.format :as tf]
      [clj-time.core :as t]
      [clojure.string :as s])
  (:gen-class))


(defn within-session?[interval-limit t1 t2]
  "Is the difference in the times within the interval limit?"
  (-> (t/interval t1 t2)
      t/in-minutes
      (<= interval-limit)))


(defn pair-neighbours [l]
  (map vector l (rest l)))


(defn index-start-of-segment-boundary [same-segment? data]
  "Find the starting indices of the segments identified by the same-segment? rule"
  (letfn [(build-segment-start-indices [acc [idx [l r]]]
            (if (same-segment? l r) acc (conj acc (inc idx))))]
    (->> data
         pair-neighbours
         (map-indexed vector)
         (reduce build-segment-start-indices [0]))))


(defn segment-ranges [same-segment? data]
  "Give the ranges of the segments identified by the same-segment? rule as vector of pairs of start (inclusive) and end (exclusive)"
  (-> (index-start-of-segment-boundary same-segment? data)
      (conj (count data))
      pair-neighbours))


(defn clean-and-parse [data]
  "Load data from source clean out invalid records and parse date time fields"
  (->> data
       (spark/map #(clojure.string/split % #"\t"))
       (spark/filter #(not-any? clojure.string/blank? (take 2 %)))
       (spark/map #(update % 1 tf/parse))
       (spark/filter #(not-any? nil? %))))


(defn- map-to-value [rdd] (spark/map (s-de/key-value-fn (fn [_ v] v)) rdd))
(defn- map-to-key [rdd] (spark/map (s-de/key-value-fn (fn [k _] k)) rdd))


(defn longest-sessions [within-session? longest-session-count data]
  (letfn [(plays-within-session? [[_ t1 & _] [_ t2 & _]] (within-session? t1 t2))

          (sub-section [plays [start end]] (take (- end start) (drop start plays)))

          (build-sessions [plays] (map (partial sub-section plays) (segment-ranges plays-within-session? plays)))]

    (let [username first
          timestamp second
          ordered-sessions (->> data
                                (spark/group-by username)
                                (spark/map-values #(sort-by timestamp %))
                                (spark/flat-map (s-de/key-value-fn (fn [_ v] (build-sessions v))))
                                (spark/map-to-pair (fn [s] (spark/tuple (count s) s)))
                                (spark/sort-by-key false)
                                (spark/zip-with-unique-id))

          top-ids (->> ordered-sessions
                       map-to-value
                       (spark/take longest-session-count)
                       set)

          unpack-track-data (comp map-to-value map-to-key)]

      (->> ordered-sessions
           (spark/filter (s-de/key-value-fn (fn [_ id] (top-ids id))))
           unpack-track-data))))


(defn track-chart [sessions]
  "Finds the 'n' most played tracks across the given sessions. Note: multi plays within a session count as only one."
  (->> sessions
       (spark/map (fn [v] (map (fn [[_ _ & track-data]] track-data) v)))
       (spark/flat-map distinct)
       (spark/group-by identity)
       (spark/map-to-pair (s-de/key-value-fn (fn [k v] (spark/tuple (count v) k))))
       (spark/sort-by-key false)
       map-to-value))


(defn analyse [sc data & {:keys [session-interval-limit longest-session-count track-chart-length] :or
                                {session-interval-limit 20 longest-session-count 50 track-chart-length 10}}]
  (->> data
       (clean-and-parse)
       (longest-sessions (partial within-session? session-interval-limit) longest-session-count)
       (track-chart)
       (spark/take track-chart-length)
       (map (fn [v] (s/join \tab v)))
       (spark/parallelize sc)))


(defn spark-context []
  (-> (conf/spark-conf)
      (conf/master "local")
      (conf/app-name "trainline-lastfm-analysis")
      spark/spark-context))


(defn -main [& args]
  (let [sc (spark-context)]
    (->> (analyse sc (spark/text-file sc (first args)))
         (spark/save-as-text-file (second args)))))