(defproject trainline-solution "0.1.0-SNAPSHOT"
  :description "Last FM Data Analysis - Steve Baird"

  :dependencies [[org.clojure/clojure "1.8.0"]
                 [gorillalabs/sparkling "2.0.0"]
                 [clj-time "0.13.0"]
                 [org.clojure/tools.trace "0.7.9"]]

  :aot [#".*" sparkling.serialization sparkling.destructuring]
  :main trainline-solution.lastfm-analysis
  :profiles {:provided {:dependencies [[org.apache.spark/spark-core_2.10 "2.1.0"]
                                       [org.apache.spark/spark-sql_2.10 "2.1.0"]]}
             :dev {:plugins [[lein-dotenv "RELEASE"]]}
             :uberjar {:aot [#".*" sparkling.serialization sparkling.destructuring]}})


