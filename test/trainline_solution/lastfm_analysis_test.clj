(ns trainline-solution.lastfm-analysis-test
  (:require [clojure.test :refer :all]
            [trainline-solution.lastfm-analysis :refer :all]
            [clj-time.core :as t]
            [clj-time.format :as tf]
            [clojure.string :as s]
            [sparkling.core :as spark]))


(def ^:dynamic sc)

(defn run-in-spark-context [f]
  (binding [sc (spark-context)]
    (f)
    (.stop sc)))

(use-fixtures :once run-in-spark-context)

(deftest new-session-test
  (testing "whether times are within session interval limit"
    (let [basetime (t/now)
          interval-limit 5]
      (is (= true (within-session? interval-limit basetime (t/plus basetime (t/minutes interval-limit)))))
      (is (= true (within-session? interval-limit basetime (t/plus basetime (t/minutes (dec interval-limit))))))
      (is (= false (within-session? interval-limit basetime (t/plus basetime (t/minutes (inc interval-limit)))))))))

(deftest index-start-of-segment-boundary-test
  (testing "find the boundaries of segments"
    (let [same-seg-rule #(< (- %2 %1) 2)]
      (is (= [0 2 4] (index-start-of-segment-boundary same-seg-rule [1 2 4 5 7 8])))
      (is (= [0] (index-start-of-segment-boundary same-seg-rule [1 2 3])))
      (is (= [0] (index-start-of-segment-boundary same-seg-rule [])))
      )))

(deftest segment-ranges-test
  (let [same-seg-rule #(< (- %2 %1) 2)]
    (is (= [[0 2] [2 4] [4 6]] (segment-ranges same-seg-rule [1 2 4 5 7 8])))
    (is (= [[0 1] [1 2] [2 3]] (segment-ranges same-seg-rule [1 3 5])))
    (is (= [[0 3]] (segment-ranges same-seg-rule [1 2 3])))
    (is (= [[0 0]] (segment-ranges same-seg-rule [])))
    ))


(deftest clean-and-parse-test
  (let [timestamp (t/now)
        timestamp-str (tf/unparse (tf/formatters :basic-date-time) timestamp)
        do-clean-and-parse (fn [data] (spark/take (count data) (clean-and-parse (spark/parallelize sc data))))
        clean-record (str "user1\t" timestamp-str "\tsong1")]

    (testing "Records with blank required fields are skipped"
      (let [data [clean-record
                  (str "\t" timestamp-str "\tsong1")
                  (str "user2\t" "\tsong1")
                  (str "user2\t" "whatever" "\tsong1")]]
        (is (= [["user1" timestamp "song1"]] (do-clean-and-parse data)))))

    (testing "Record with unparseable date field is skipped"
      (let [data [clean-record
                  (str "user2\t" "whatever" "\tsong1")]]
        (is (= [["user1" timestamp "song1"]] (do-clean-and-parse data)))))

    (testing "Record with missing fields is skipped"
      (let [data [clean-record
                  (str "user2")]]
        (is (= [["user1" timestamp "song1"]] (do-clean-and-parse data)))))
    ))


(deftest longest-sessions-test
  (let [timestamp1 (t/now)
        timestamp2 (t/plus timestamp1 (t/minutes 1))
        data [["user1" timestamp1 "song1"]
              ["user1" timestamp2 "song2"]
              ["user1" timestamp2 "song3"]
              ["user2" timestamp1 "song4"]
              ["user2" timestamp1 "song5"]
              ["user2" timestamp2 "song6"]]]

    (is (= #{[["user1" timestamp2 "song2"] ["user1" timestamp2 "song3"]]
            [["user2" timestamp1 "song4"] ["user2" timestamp1 "song5"]]}

           (set (spark/take 2 (longest-sessions = 2 (spark/parallelize sc data))))))))


(deftest track-chart-test
  (let [timestamp (t/now)
        sessions [[["user" timestamp "song1"]
                   ["user" timestamp "song3"]
                   ["user" timestamp "song2"]]
                  [["user" timestamp "song3"]
                   ["user" timestamp "song2"]
                   ["user" timestamp "song3"]]]]
    (is (= [["song3"] ["song2"] ["song1"]] (spark/take 3 (track-chart (spark/parallelize sc sessions)))
  ))))


(deftest analyse-test
  (let [expected-result (s/split-lines (slurp "test/trainline_solution/sample-out.tsv"))]
    (is (= expected-result (spark/take 10 (analyse sc (spark/text-file sc "test/trainline_solution/sample-in.tsv")))))))