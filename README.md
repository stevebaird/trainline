# Analysis of Last FM Data

I have used Apache Spark with the Clojure Sparkling library hosted in the Google Cloud Platform
to analyse the Last FM data.
The data is analysed by performing the following process

1. Load data file into Spark
1. Clean and parse the data removing any bad records.
1. Group the data by user and sort the play lists by time.
1. For each (user, ordered play list) pair group the plays into sessions.
1. Order the sessions by number of plays and discard all but the longest 50.
1. The tracks in the top 50 sessions are counted, ignoring multiple plays within a session.
1. The top 10 tracks with most plays are selected and written to the specified output folder.

A file of output from running the full Last FM data set against the criteria asked for can be found in `Top10Tracks.tsv`

Assumptions made:
- multi plays within a single user session count as only one when calculating top 10 tracks across the top 50 sessions.
- there is some non-determinism in the implementation in that the top 50 and top 10 cut offs are applied
arbitrarily if there are joint places.

I spent a short time attempting to optimise the processing by repartitioning after the user grouping and after reducing
down to 50 sessions but only saw a performance hit from the shuffling.

## Requirements

- Lein
- Java 8
- Google Cloud Platform Command Line (to run remotely)

## Test

Run unit tests with:

```
lein test
```

## Build

To create uber jar for running remotely:

```
lein uberjar
```

## Run Locally

The analysis can be run locally as follows:

```
lein run <path to input file> <path to output dir>
```

for example

```
lein run test/trainline_solution/sample-in.tsv out
```

## Running on Google Cloud Platform

1) Login and initialise your GCP session

```
gcloud init
```

2) Copy the data file for analysis to a GCP bucket.

```
gsutil cp <source file> <destination bucket>
```

3) Copy Uber Jar to a GCP bucket.

```
gsutil cp target/trainline-solution-0.1.0-SNAPSHOT-standalone.jar <destination bucket>
```

4) Submit analysis task to a GCP cluster.

```
gcloud dataproc jobs submit spark --cluster <cluster-name> --jar gs://<bucket name>/trainline-solution-0.1.0-SNAPSHOT-standalone.jar -- gs://<bucket name>/<data file> gs://<bucket name>/<out folder>
```